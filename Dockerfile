# docker metadata
FROM node:18.7.0 
EXPOSE 3000

RUN groupadd --gid 1627 ujjval \
    && useradd --uid 1627 --gid 1627 -m ujjval 
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.

WORKDIR /app
RUN chown -R ujjval:ujjval /app

USER ujjval

COPY --chown=ujjval:ujjval package.json package-lock*.json ./

RUN npm install \
    && npm install nodemon --save-dev 

COPY --chown=ujjval:ujjval . .

RUN npm run coverage-lcov

CMD [ "node", "server.js" ]
